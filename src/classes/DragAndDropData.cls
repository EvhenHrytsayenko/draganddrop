/**
 * Created by Pruha on 22.06.2021.
 */

public with sharing class DragAndDropData {
    @AuraEnabled(Cacheable=true)
    public static List<Account> getAccounts() {
        return [SELECT Id, Name FROM Account LIMIT 5];
    }
}