import { LightningElement, track } from 'lwc';
import getAccounts from "@salesforce/apex/DragAndDropData.getAccounts";

export default class Secondary extends LightningElement {
    @track items1 = [];
    @track items2 = [];
    item;

    constructor() {
        super();
        getAccounts()
            .then(result => {
                result.forEach(item=> {
                    this.items1.push(item.Name)
                    this.items2.push(item.Fax)
                    });
            })
            .catch(error => {
                this.error = error;
            });
    }

    allowDrop(event) {
        event.preventDefault();
    }

    dragStart(event) {
        this.item = event.target;
    }

    drop(event) {
         if (event.target.id === this.template.querySelectorAll('.dragAndDrop')[0].id) {
             this.items1.push(this.item.textContent);
             this.item.remove();
         }
         if (event.target.id === this.template.querySelectorAll('.dragAndDrop')[1].id) {
             this.items2.push(this.item.textContent);
             this.item.remove();
        }
    }

    onclick(event) {
        var item = event.target;
        item.parentNode.remove();
    }
}